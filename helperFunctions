#!/bin/bash 

# Echo to stderr
echoErr() { echo "$@" >&2; }

# Test if required CMake file exists in the project
checkCMakeProject () {
    if [[ -z ${CMAKEBYPASS+x} ]]; then  # http://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash 
        if [[ -e ./CMakeLists.txt ]]; then
            if [[ ! `grep -e "^add_llvm_loadable_module("$PROJECT"\b" CMakeLists.txt` ]]; then
                echo "CMakeLists.txt does not have your project related settings"
                exit 1
            fi
        else
            echo "CMakeLists.txt does not exist"
            exit 1
        fi
    fi
}

# Test if the given project is integrated in the LLVM CMake file
checkCMakeIntegrated() {
    if [[ ! `grep -e "^add_subdirectory("$PROJECT")" $LLVMHOME/lib/$MODE/CMakeLists.txt` ]]; then
        echo "add_subdirectory("$PROJECT")" >> "$LLVMHOME"/lib/"$MODE"/CMakeLists.txt
        echoErr "Added project directory in LLVM directory"
    fi
}

checkValidInputs() {
    local nargs=$1
    if [ $nargs -eq 0 ]; then
        echoErr "Needs atleast one test file to run the pass - $PASSNAME"
        usage
        exit 1;
    fi
}

setupProject() {
    readonly local includeDir="$LLVMHOME"/include/llvm/"$MODE"/"$PROJECT" # Directory where include files go
    readonly local libDir="$LLVMHOME"/lib/"$MODE"/"$PROJECT" #Directory where cpp and CMakeFiles go
    readonly local sourceDir=./
    checkCMakeProject
    # Setup the project in the LLVM source directory
    mkdir -p $includeDir
    rsync -avz --include="*/" --include="*.hpp" --exclude="*" --prune-empty-dirs $sourceDir $includeDir > /dev/null # Send include files
    mkdir -p $libDir 
    rsync -avz --include="*/" --include="*.cpp" --include="CMakeLists.txt" --exclude="*" --prune-empty-dirs $sourceDir $libDir > /dev/null # Send rest of the files
    #rsync -avz --exclude="runMe" --exclude="test" --exclude="*~" ./ "$LLVMHOME"/lib/"$MODE"/"$PROJECT/" >/dev/null
    checkCMakeIntegrated
}

buildProject() {
    # Build LLVM 
    mkdir -p "$LLVMHOME/build"
    pushd "$LLVMHOME/build" >/dev/null
    cmake -G "Ninja" ../ >/dev/null
    ninja 
    popd >/dev/null
}

installBin() {
    # Install LLVM binaries
    pushd "$LLVMHOME/build" >/dev/null
    ninja install >/dev/null
    popd >/dev/null
}


cmdline() {
    # got this idea from here:
    # http://kirk.webfinish.com/2009/10/bash-shell-script-to-use-getopts-with-gnu-style-long-positional-parameters/
    local arg=
    for arg
    do
        local delim=""
        case "$arg" in
            #translate --gnu-long-options to -g (short options)
            --cmakeBypass)    args="${args}-c ";;
            --verify)         args="${args}-v ";;
            --debug)          args="${args}-d ";;
            --time)           args="${args}-t ";;
            --valgrind)       args="${args}-l ";;
            --help)           args="${args}-h ";;
            #pass through anything else
            *) [[ "${arg:0:1}" == "-" ]] || delim="\""
                args="${args}${delim}${arg}${delim} ";;
        esac
    done

    #Reset the positional parameters to the short options
    eval set -- $args

    while getopts "cvdtlh" OPTION
    do
         case $OPTION in
         c)  readonly CMAKEBYPASS=true
             ;;
         v)
             readonly VERIFY="-verify"
             ;;
         d)
             readonly DEBUG="--debug-pass=Structure"
             ;;
         t)
             readonly TIMEPASSES="-time-passes"
             ;;
         l)
             readonly VALGRIND="valgrind"
             ;;
         h)
             usage
             exit 0
             ;;
        \?)
            usage
            exit 1
            ;;
        esac
    done

    shift "$((OPTIND-1))" # Shift off the options and optional
    checkValidInputs $#
    ARGS=$@
    NARGS=$#

    #if [[ $recursive_testing || -z $RUN_TESTS ]]; then
        #[[ ! -f $CONFIG_FILE ]] \
            #&& eexit "You must provide --config file"
    #fi
    return 0
}

usage() {
    cat <<EOF
usage: $0 [-vdtlh] FILE1 FILE2 ..
where 
-c: Bypass cmake check. Use for passes that has to be integrated with LLVM
-v: Verify the pass using -verify
-d: Pass --debug-pass=STRUCTURE to opt
-t: Get timing information of the passes
-l: Debug the pass using valgrind
-h: Print this message
EOF
}

run() {
     #Run test program
    for i in $*; do
        $LLVMHOME/bin/bin/clang -S -emit-llvm "$i" -o "$i".ll
        $VALGRIND $LLVMHOME/bin/bin/opt -load $LLVMHOME/bin/lib/"$PROJECT".so $VERIFY -"$PASSNAME" $DEBUG --disable-output $TIMEPASSES "$i".ll
    done
}

